const Bio = () => (
  <div>
    <div className="bio">
      <img
        src="/static/images/profile.jpeg"
        alt="Danny Pule - Software Engineer"
      />
      <h2>
        <span>Danny Pule</span>
      </h2>
      <h5>Software Engineer, South West London</h5>
      <h6>Creating scalable and testable experiences for the Web. </h6>
    </div>
    <div className="site-section how-i-do">
      <div className="section-inner">
        <h1>~ About ~</h1>
        <p>
          {`
            When I'm not building something or learning about some new web technology you can find me 
            scrolling through Dribbble.com appreciating the designs contained therein.
          `}
        </p>
      </div>
    </div>
    <div className="site-section skillz">
      <div className="section-inner">
        <div>
          <h1>~ Technologies I Work With ~</h1>
          <p>React, Redux, Typescript,</p>
          <p>HTML, CSS, CSS-in-JS,</p>
          <p>Jest, Cypress,</p>
          <p>Node.js, PostgreSQL,</p>
          <p>CI/CD, Docker, Jenkins</p>
        </div>
      </div>
    </div>
  </div>
);

export default Bio;
