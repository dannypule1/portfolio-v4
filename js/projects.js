export default [
  {
    imageUrl: `/static/images/enhance.svg`,
    title: `Enhance.com (2019)`,
    summary: `Web Hosting Management, Made Simple`,
    contribution: `Initial setup of the codebase including the best practices and design
    system that will be used to create enhance.com's new Web hosting management platform. In this role I work closely with the company founders,
    Lead Product Designer and other Senior Engineers to create new features for Enhance's platform. To do that we're using React, Typescript, 
    Redux & CSS-in-JS.`,
    skillsUsed: `React, Redux, Typscript, Jest Testing, Continuous Integration (Gitlab CI), Agile, HTML, CSS-in-JS`,
    className: `enhance-image`,
  },
  {
    imageUrl: `/static/images/nutmeg-logo.png`,
    title: `Nutmeg.com (2018 & 2019)`,
    summary: `Europe's largest digital wealth manager`,
    contribution: `I was part of the sub-team that delivered Nutmeg’s new Socially Responsible Investing offering in Nov 2018.
    Frequent presentations and demos to engineering colleagues about new tools and development strategies at our bi-weekly Web Architecture meeting`,
    skillsUsed: `React, Redux, Jest Testing, Continuous Integration (Jenkins), Agile, HTML, CSS`,
    className: `arc-image`,
  },
  {
    imageUrl: `/static/images/arcadia.gif`,
    title: `Website for Arcadia's Fashion Brands (2017 & 2018)`,
    summary: `Topshop.com, Topman.com other brands.`,
    contribution: `This project touches on various aspects of the user journey
    from product listing views to checkout. I'm a member of a cross-functional team which includes Javascript Engineers, UX
    Designers, Quality Assurance, Business Analysts & Product Owners. `,
    skillsUsed: `React, Redux, Jest Testing, Git, Agile, HTML, CSS`,
    className: `arc-image`,
  },
  {
    imageUrl: `/static/images/ri-client-dashboard.png`,
    title: `Data Visualisation Interface (2017)`,
    summary: `A user interface for RI's Data Science team's analysis of client's retail data.`,
    contribution: `I was involved in this project from the early planning phases. I was the sole Software Engineer for this dashboard application where I worked with a team of .Net and Database engineers based overseas.
    I received some initial wireframes for the UI and I took them to the next level by implementing a glossy interface using Material Design.`,
    skillsUsed: `Angular 4, Redux, Typescript, Node.js, Webpack, Git, Material Design, UX, Agile`,
  },
  {
    imageUrl: `/static/images/osca.png`,
    title: `Analytics Dashboards (2016 & 2017)`,
    summary: `A minimum viable product used to market RI's retail analytics programmes to new clients.`,
    contribution: `In this project, I worked with a UX Designer/Developer to build the frontend of this prototype application.
    The outcome of the protoype was that RI was able to secure a multi-million GBP contract with a large retailer.`,
    skillsUsed: `Angular 1.x, Javascript, Node.js, Gulp, Git, Material Design, UX, Agile`,
  },
  {
    imageUrl: `/static/images/screen_appsme.png`,
    title: `Appsme App Builder (2016)`,
    summary: `A wix.com-like app builder which enables small business owners to create an iOS and Android app for their
    business. The online mobile app builder made heavy use of Javascript.`,
    contribution: `Adding new functionality and improving performance of Appsme's mobile app builder.`,
    skillsUsed: `Javascript, Git, HTML, CSS, Less`,
  },
  {
    imageUrl: `/static/images/appsme_getstarted.png`,
    title: `Appsme Landing Page (2016)`,
    summary: `A landing page for Appsme marketing campaigns.`,
    contribution: `Layout of page elements using CSS. Optimizing for various screen sizes. Adding functionality to admin
    portal using PHP and Javascript`,
    skillsUsed: `Javascript, PHP, Git, HTML, CSS, Less`,
  },
  {
    imageUrl: `/static/images/screen-kiqplan-app.png`,
    maxWidthForImage: true,
    title: `Kiqlan Mobile App v2 (2015)`,
    summary: `A personal training app which is part of Kin Wellness's (formerly Fitbug) corporate wellness program.`,
    contribution: `Pixel perfect CSS to match the look and feel of the design briefs.`,
    skillsUsed: `HTML, CSS, Sass, Git`,
  },
  {
    imageUrl: `/static/images/screen-kiqplan.png`,
    title: `Kiqlan Website (2015)`,
    summary: `A sales and user registration website for users of Kiqplan mobile app.`,
    contribution: `SEO optimization, improving layout of existing pages, enhancing registration
    functionality, creating new backend tools for user management and bug fixing.`,
    skillsUsed: `Javascript, JQuery, PHP, AJAX, Git, HTML, CSS, Sass`,
  },
  {
    imageUrl: `/static/images/screen-kiqplan-support.png`,
    title: `Kiqplan Support Center (2014)`,
    summary: `A ticketing and help centre for Kiqplan users.`,
    contribution: `I heavily customized a very basic theme which only looked good on desktops and made it mobile ready.
    The look and feel of the site was also given an overhaul to bring it more in line with the Kiqplan branding.`,
    skillsUsed: `Javascript, HTML, CSS`,
  },
];
